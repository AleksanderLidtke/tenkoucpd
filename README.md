# README #

### What is this repository for? ###

This is a set of functions to interact with the Charged particle Detector (CPD) on-board
the Ten-Koh satellite. The CPD is a stand-alone part with number TK-05-05 and can be
accessed on the *SPI_PL* bus controlled by two PIC16F877 PICs, the main and backup
experiment controller units (ECU_M and ECU_B, respectively) located in the TK-05-01-01 PCB.

### How do I get set up? ###

Just clone this repository as well as the [TenkouSPIMaster library](https://bitbucket.org/rafarodleon/tenkouspimaster)
that is used to send and receive data on the SPI bus.

There should be separate header and source files with all the functions needed to read
and control the CPD, as well as a set of dedicated test functions in a separate `test.c` file.
The header and source files will be included in the ECU software, while the test functions
will only be used to test the code and the hardware on the ground.

### Who do I talk to? ###

For more info, talk to Isai or Alek on Slack.