/*
 * File:   test.c
 * Author: alek
 *
 * Created on 08 February 2018, 14:06
 */

// CONFIG BITS
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#define _XTAL_FREQ     20000000

// Includes.
#include <xc.h>
#include "CPD.h"
#include "uart.h" // TenkouUART lib needed for debug trace.
#include "sd.h" // To test the timing.

/*
 * Various defines specific for debugging and TK-01-01-01 hardware.
 */
#define SPI_BUF_SIZE 16 // Size of the read and write SPI buffers in bytes.
                        // Corresponds to Liulin status message.

// Bus frequencies and timing definitions.
#define UART_BAUD 9600 // UART baud rate in bits per second.
#define I2C_FREQ 100000 // I2C frequency in Hz.
#define MAIN_LOOP_WAIT 10000 // How long to wait after every main loop in ms.

// Hardware-specific pin definitions, same as on the TK-05-01-01 ECU PCB.
#define SPI_PL_CS_SD_INIT TRISD7 // SD card CS is defined in a struct, only initialise.

/*
 * Data buffers used for debugging.
 */
unsigned char loopStart[] = {"START"};
unsigned char cpdMsg[]={"CPD"}; // Make the CPD and Liulin data stand out.
unsigned char liuMsg[]={"LIU"};
unsigned char sdMsg[]={"SD"};
unsigned char errData[] = {"ERR"}; // Error notifier.
unsigned char uartTerminator[] = {"XXX\n"}; // Distinct end of debug messages.
unsigned char spiRData[SPI_BUF_SIZE] = {'0'}; // Array to store received SPI data.

uint8_t testCPD(void)
/* Test the CPD. */
{
    uint8_t ret = cpdReset(); // Hardware reset, has to be sent after CPD power up.
    
    ret=cpdSetAllThresholds(0xFF);
    for(uint16_t i=0; i<500; i++) // Get CPD data 500 times to empty the FIFO.
    {
        ret=cpdFifoRead(spiRData); // Get four bytes of data for this sensor.
        uartWriteBytes(cpdMsg,3); // Print the CPD data identifier.
        uartWriteBytes(spiRData,CPD_BUF_SIZE); // Only print the read CPD data.
        uartWriteBytes(uartTerminator,1);
    }
    // Set threshold and read another channel.
    ret=cpdSetThreshold(2,7);
    ret=cpdFifoRead(spiRData); // Get four bytes of data for this sensor.
    uartWriteBytes(cpdMsg,3); // Print the CPD data identifier.
    uartWriteBytes(spiRData,CPD_BUF_SIZE); // Only print the read CPD data.
    uartWriteBytes(uartTerminator,1);
    return 0;
}

//TODO find the space for the Liulin data chunk.
//unsigned char liuData[LIU_BUF_SIZE]={'0'}; // Data returned by the Liulin.
uint8_t testLiu(void)
/* Test the Liulin. */
{
    // The test function in Matlab doesn't work either. Doug said it doesn't work yet.
    //ret = liulinTest(spiRData); // Read first 12 preamble and last 4 health bytes.
    
    uartWriteBytes(liuMsg,3); // Print the Liulin data identifier.
    // Print the whole buffer, Liulin data buffer size will change in the future.
    uartWriteBytes(spiRData,SPI_BUF_SIZE);
    uartWriteBytes(uartTerminator,1); // Mark the end of the message.
    
    // Read the Liulin. if DEBUG_LIULIN is defined, print all of its data to UART.
    uint8_t ret = liulinRead(spiRData);
    uartWriteBytes(liuMsg,3);
    uartWriteBytes(spiRData,SPI_BUF_SIZE);
    uartWriteBytes(uartTerminator,1);
    
    return ret;
}

void main(void)
/* Run the tests of the Liulin and the CPD in a loop. */
{
    // Configure CSes as outputs and initialise as high.
    SPI_PL_CS_LIU_INIT=0;
    SPI_PL_CS_CPD_INIT=0;
    SPI_PL_CS_SD_INIT=0;
    SPI_PL_CS_LIU=1;
    SPI_PL_CS_CPD=1;
    
    uint8_t ret=9; // Return status of various tests.
    
    // Define where the SD car CS is.
    sd_communication_information_t sd_condition;
    sd_condition.port = &PORTD;
    sd_condition.pin = 6; // RD7.
    sd_condition.data_size = 4;
    
    spiMasterInit(); // Start the SPI as master.
    
    // Show the beginning of the main loop by sending a string.
    uartInit(UART_BAUD); // Initialise UART @ 9600 baud.
    uartWriteBytes(loopStart,5);
    
    //TODO find a way to reduce the instantaneous amount of data returned by the Liulin,
    // e.g. by reading it in chunks, reducing the resolution or combining channels.
    // Test the Liulin
    //ret = testLiu();//TODO get this to work again.
    
    // Test the CPD
    ret = testCPD();
    
    //TODO tell Doug our frequency is now 147 kHz, and send him a timing diagram.
    //TODO ask Doug why the input to the CPD from Matlab is 3.3 V level. Is this a problem?
    //TODO ask Doug how to select the sensor, because we cannot do this with the Matlab scripts.
    //TODO ask Doug why the returned threshold is always 1F, even though we try to set it with Matlab?
    //TODO in the first loop the CPD returns 4 bytes correctly, but then the reset cannot be heard and the bytes are 0x20 or 0x2b. Why?
    
    //TODO add writing data to and reading from the SD card.
    /*
    sd_setup_SPImode(sd_condition);
    for(size_t address=1; address<sizeof(spiRData); address++)
    {
        sd_condition.address_number = (uint32_t)address;
        Write_SDcard(sd_condition,spiRData[address]);
    }
    uartWriteBytes(sdMsg,2); // SD-read data identifier.
    for(size_t address=1; address<sizeof(spiRData); address++)
    {
        sd_condition.address_number = (uint32_t)address;
        Read_SDcard(sd_condition,spiRData[address]);
    }*/
    
    // End main loop.
    uartWriteBytes(uartTerminator,4); // Distinct end of the data message.
    __delay_ms(MAIN_LOOP_WAIT);
}
