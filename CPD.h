/* 
 * File:   CPD.h
 * Author: alek
 *
 * Created on 08 February 2018, 14:05
 */

/* SPI Master pins allocation and corresponding CPD pins:
 * - SPI_MOSI_MASTER RB3 = G
 * - SPI_MISO_MASTER RB1 = H
 * - SPI_CK_MASTER   RB2 = F
 */
#ifndef CPD_H // Header safeguard.
#define	CPD_H

//#define DEBUG_LIULIN // To print every Liulin byte to UART for eyballing.

#include "spi_master.h" // Needed to communicate with the CPD.
#include <stdint.h> // For uint8_t.
#ifdef DEBUG_LIULIN
    #include "uart.h"
#endif

//TODO discard most of the data to reduce instantaneous PIC memory burden.
#define LIU_BUF_SIZE 16// 528 // Liulin always returns 528 bytes.
#define CPD_BUF_SIZE 4 // Size of one CPD reading in bytes.

#define DELAY_LIULIN_MS 5000 // Wait duration between commands to and data from Liulin.
#define DELAY_CPD_MS 2 // Wait duration between consecutive transactions with the CPD.
#define DELAY_CPD_AFTER_CS_MS 3 // After CS falling edge before the transaction.
#define DELAY_CPD_BEFORE_CS_MS 6 // After the transaction and before CS rising edge.

// Hardware-specific pin definitions, same as on the TK-05-01-01 ECU PCB.
#define SPI_PL_CS_CPD RD0 // CS for the CPD, D in the CPD connector.
#define SPI_PL_CS_CPD_INIT TRISD0
#define SPI_PL_CS_LIU RD1 // CS for the liulin detector, E in the CPD connector.
#define SPI_PL_CS_LIU_INIT TRISD1

// Liulin and CPD command bytes as expected by the SPI Master library.
unsigned char cpdCommand_reset=0xA0;
unsigned char cpdCommand_setTh=0xA1;
unsigned char cpdCommand_reqData=0xA2;
unsigned char liulinCommand_test=0x54; // T
unsigned char liulinCommand_reqData=0x4D; // M

uint8_t cpdReset(void);
uint8_t cpdSetAllThresholds(uint8_t threshold);
uint8_t cpdSetThreshold(uint8_t sensorId, uint8_t threshold);
uint8_t cpdFifoRead(uint8_t* dataArray);

uint8_t liulinTest(unsigned char* bufferArray);
uint8_t liulinRead(unsigned char* bufferArray);

#endif	/* CPD_H */

