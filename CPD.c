/*
 * File:   CPD.c
 * Author: alek
 *
 * Created on 08 February 2018, 14:06
 */

#include "CPD.h"

uint8_t cpdReset(void)
/* Perform hardware reset of the CPD. Return 0 if everything goes OK. */
{
    SPI_PL_CS_CPD=0; // CS low.
    __delay_ms(DELAY_CPD_AFTER_CS_MS); // Satisfy the required timing.
    spiMasterWrite(&cpdCommand_reset); // System reset command byte.
    __delay_ms(DELAY_CPD_BEFORE_CS_MS); // Satisfy the required timing.
    SPI_PL_CS_CPD=1; // CS back high.
    __delay_ms(DELAY_CPD_MS); // Wait before the next transaction.
    return 0;
}

uint8_t cpdSetAllThresholds(uint8_t threshold)
/* Set the thresholds for all eight sensors,  the threshold being [0 to 255].
 * Return 0 if everything goes well.
 */
{
    uint8_t ret=0; // Will return this.
    uint8_t temp=9; // Return value for each sensor.
    for(uint8_t id=0; id<8; id++) // For sensor ID.
    {
        temp=cpdSetThreshold(id,threshold);
        if(temp!=0) // Not OK.
        {
            ret=temp;
        }
    }
    return ret;
}

uint8_t cpdSetThreshold(uint8_t sensorId, uint8_t threshold)
/* Set threshold for the given CPD sensor with index [0 to 7], the threshold
 * being [0 to 255].
 * Return 0 if everything goes OK.
 */
{
    SPI_PL_CS_CPD=0; // CS low.
    __delay_ms(DELAY_CPD_AFTER_CS_MS); // Satisfy the required timing.
    spiMasterWrite(&cpdCommand_setTh); // Send the command byte.
    __delay_ms(DELAY_CPD_BEFORE_CS_MS); // Satisfy the required timing.
    SPI_PL_CS_CPD=1; // CS back high.
    
    __delay_ms(DELAY_CPD_MS); // Wait before the next transaction.
    
    SPI_PL_CS_CPD=0;
    __delay_ms(DELAY_CPD_AFTER_CS_MS);
    spiMasterWrite(&sensorId);
    __delay_ms(DELAY_CPD_BEFORE_CS_MS);
    SPI_PL_CS_CPD=1;
    
    __delay_ms(DELAY_CPD_MS);
    
    SPI_PL_CS_CPD=0;
    __delay_ms(DELAY_CPD_AFTER_CS_MS);
    spiMasterWrite(&threshold);
    __delay_ms(DELAY_CPD_BEFORE_CS_MS);
    SPI_PL_CS_CPD=1;
    
    __delay_ms(DELAY_CPD_MS); // Wait before the next transaction.
    return 0;
}

//TODO might want to call cpdRead many times to get a time trace of data.
//TODO might also want to discard some of the data, like status bytes or thresholds.

uint8_t cpdFifoRead(uint8_t* dataArray)
/* Read the CPD FIFO queue and record the four response bytes in dataArray: 
 * status byte, sensorID, threshold, data.
 * Return 0 if everything goes OK.
 */
{
    //TODO: if 1st byte is XXXXXXX1, FIFO is empty and there are no data to be read. Handle this.
    
    SPI_PL_CS_CPD=0; // CS low.
    __delay_ms(DELAY_CPD_AFTER_CS_MS); // Satisfy the required timing.
    spiMasterWrite(&cpdCommand_reqData); // Send the command byte.
    __delay_ms(DELAY_CPD_BEFORE_CS_MS); // Satisfy the required timing.
    SPI_PL_CS_CPD=1; // Bring high after every transfer.
    
    __delay_ms(DELAY_CPD_MS); // Wait before consecutive transactions.
    
    SPI_PL_CS_CPD=0;
    __delay_ms(DELAY_CPD_AFTER_CS_MS);
    spiMasterRead(dataArray);
    __delay_ms(DELAY_CPD_BEFORE_CS_MS);
    SPI_PL_CS_CPD=1;
    
    __delay_ms(DELAY_CPD_MS);
    
    SPI_PL_CS_CPD=0;
    __delay_ms(DELAY_CPD_AFTER_CS_MS);
    spiMasterRead(dataArray+1);
    __delay_ms(DELAY_CPD_BEFORE_CS_MS);
    SPI_PL_CS_CPD=1;
    
    __delay_ms(DELAY_CPD_MS);
    
    SPI_PL_CS_CPD=0;
    __delay_ms(DELAY_CPD_AFTER_CS_MS);
    spiMasterRead(dataArray+2);
    __delay_ms(DELAY_CPD_BEFORE_CS_MS);
    SPI_PL_CS_CPD=1;
    
    __delay_ms(DELAY_CPD_MS);
    
    SPI_PL_CS_CPD=0;
    __delay_ms(DELAY_CPD_AFTER_CS_MS);
    spiMasterRead(dataArray+3);
    __delay_ms(DELAY_CPD_BEFORE_CS_MS);
    SPI_PL_CS_CPD=1;
    return 0;
}

uint8_t liulinTest(unsigned char* bufferArray)
/* Read the test data of the Liulin and write them into the input array. 
 * Return 0 if everything goes well.
 * 
 * Note: this doesn't work, the sensor returns invalid bytes. But the Matlab
 * test function doesn't work either.
 */
{
    unsigned char tempData=0; // Each byte read from Liulin.
    //TODO temporary, aiming to reduce the instantaneous amount of data held in PIC memory.
    uint8_t ctr=0; // How many bytes have been stored in bufferArray.
    
    SPI_PL_CS_LIU=0; // CS low.
    spiMasterWrite(&liulinCommand_test); // Write T i.e. 0x54 to start the test.
    __delay_ms(DELAY_LIULIN_MS); // Wait before reading the data.
    
    // Read the response bytes one by one.
    for(uint16_t i=0; i<528; i++)
    {
        spiMasterRead(&tempData);
        //TODO temporary, aiming to reduce the instantaneous amount of data held in PIC memory.
        if(i<12 || i>523) // Only look at the first response and health data.
        {
            bufferArray[ctr]=tempData;
            ctr++;
        }
        //else - ignore the test data to save memory.
    }
    SPI_PL_CS_LIU=1; // CS back high.
    return 0;
}

uint8_t liulinRead(unsigned char* bufferArray)
/* Read the data of the Liulin and write them into the input array. 
 * Return 0 if everything goes well.
 */
{
    unsigned char tempData=0; // Each byte read from Liulin.
    //TODO temporary, aiming to reduce the instantaneous amount of data held in PIC memory.
    uint8_t ctr=0; // How many bytes have been stored in bufferArray.
    
    SPI_PL_CS_LIU=0; // CS low.
    spiMasterWrite(&liulinCommand_reqData); // Write T i.e. 0x54 to start the test.
    __delay_ms(DELAY_LIULIN_MS); // Wait before reading the data.
    
    // Read the response bytes one by one.
    for(uint16_t i=0; i<528; i++)
    {
        spiMasterRead(&tempData);
        //TODO temporary, aiming to reduce the instantaneous amount of data held in PIC memory.
        if(i<12 || i>523) // Only look at the first response and health data.
        {
            bufferArray[ctr]=tempData;
            ctr++;
        }
        //else - ignore the test data to save memory, unless we're debugging.
        #ifdef DEBUG_LIULIN
            uartWrite(&tempData); // Eyeball all the Liulin data.
        #endif
    }
    SPI_PL_CS_LIU=1; // CS back high.
    return 0;
}